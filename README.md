# Chi-square

Python implementation of calculating chi-square value.

<b>Command line params:</b>
<br>`python main.py <k> <alpha> [--digit_num]`

<b>Examples:</b>
<br><br><b>Input:</b>
<br> `python main.py 1 0.99 --digit_num 15`
<br><b>Output:</b>
<br>`0.0006733976829692741`
<br><br><b>Input:</b>
<br>`python main.py 1 0.99 --digit_num 105`
<br><b>Output:</b>
<br>`0.0002410264412936418`
<br><br><b>Input:</b>
<br>`python main.py 1 0.99 --digit_num 135`
<br><b>Output:</b>
<br>`0.00022296687737033655`
<br><br><b>Input:</b>
<br>`python main.py 1 0.01`
<br><b>Output:</b>
<br>`6.635730271227658`
<br><br><b>Input:</b>
<br>`python main.py 30 0.6`
<br><b>Output:</b>
<br>`27.441263525397517`
<br><br><b>Input:</b>
<br>`python main.py 2 0.99`
<br><b>Output:</b>
<br>`0.020100671706998696`
<br><br><b>Input:</b>
<br>`python main.py 6 0.01` 
<br><b>Output:</b>
<br>`16.812593624927104`
<br><br><b>Input:</b>
<br>`python main.py 6 0.01 --digit_num 15` 
<br><b>Output:</b>
<br>`16.812360374085813`
<br><br><b>Input:</b>
<br>`python main.py 6 0.01 --digit_num 5`
<br><b>Output:</b>
<br>`16.8121337890625`

