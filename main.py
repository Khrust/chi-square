"""This file contains an implementation of calculating chi-square values
"""
import math
import argparse


def probability_density_function(x, k):
    """This function computes probability using specified values x and k.
    :param x: (float) value of sample;
    :param k: (int) degrees of freedom.
    :return:
    """
    if x <= 0:
        return 0

    half_k = k / 2.0
    res = 0
    if k > 2:

        a = math.exp(-x / 2)
        b = math.pow(x, half_k - 1)
        c = math.pow(2, half_k)
        d = math.gamma(half_k)

        res = a * b / (c * d)
    elif k == 2:
        res = 1 / (2 * math.exp(x/2))
    elif k == 1:
        res = 1 / (math.sqrt(2 * x * math.exp(x)) * math.gamma(half_k))
    return res


def critical_chisqr(k, alpha, digit_num=10):
    """This function calculates critical chi squared value.
    :param k: (int) degrees of freedom;
    :param alpha: (float) confidence.
    :param digit_num: (int) set precision of number of digits after point in computations.
    :return: (float) critical chi-square.
    """
    # value at which probability_density_function tends to zero. Selected experimentally.
    b = 70

    # fast calculation of critical chi-square for k=2.
    if k == 2:
        lower_lim_x = b - 2 * math.log(1 + alpha * math.sqrt(math.exp(b)), math.e)
        return lower_lim_x
    if alpha == 1:
        return 0

    # init searching range.
    right_boundary = b
    left_boundary = 0

    # init upper limit of integration.
    upper_lim_x = b

    # set the precision
    eps = math.pow(10, -digit_num)

    # init number of terms of integral sum.
    steps = digit_num * 3800

    # init critical chi-square value.
    lower_lim_x = 0

    # init delta
    delta = 1

    # We will search critical chi-square value until
    # left_boundary and right_boundary are equal or delta will be acceptable.
    while (left_boundary < right_boundary) and (not ((delta <= eps) and (delta >= 0))):

        # calculate middle of the range.
        lower_lim_x = left_boundary + ((right_boundary - left_boundary) / 2)

        # calculate increment by x-axis.
        dx = (upper_lim_x - lower_lim_x) / steps

        # calculate space under graph of distribution's density in the our range.
        space_under_graph = 0
        for i in range(steps + 1):
            space_under_graph += (probability_density_function(lower_lim_x + i * dx, k) * dx)

        # calculate difference between actual space under graph and expected value.
        delta = space_under_graph - alpha

        should_move_left = ((delta < eps) and (delta < 0))
        should_move_right = ((delta > eps) and (delta > 0))

        if should_move_left:
            right_boundary = lower_lim_x
        elif should_move_right:
            left_boundary = lower_lim_x

    return lower_lim_x


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This program computes critical chi-square value.')
    parser.add_argument('k', type=int, help='Degrees of freedom.')
    parser.add_argument('alpha', type=float, help='Confidence (0;1).')
    parser.add_argument('--digit_num', default=10, type=int, help='Precision of computations.')
    args = parser.parse_args()

    print(critical_chisqr(args.k, args.alpha, args.digit_num))
